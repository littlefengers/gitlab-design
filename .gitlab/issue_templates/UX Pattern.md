### Problem

(What’s the problem that this pattern solves? Why is it worth solving?)

### Solution

(What’s the solution? Why is it like that? What are the benefits?)

### Example(s)

(One or more images showing the UX pattern. They don’t have to be in GitLab.)

### Usage

(When do you use this pattern? And how?)

#### Dos and dont's

(Use this table to add images and text describing what’s ok and not ok.)

| :white_check_mark:  Do | :stop_sign: Don’t |
|------------------------|-------------------|
|  |  |

### Related patterns

(List any related or similar solutions. If none, write: No related patterns)

### Links / references

### Pattern checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit, if applicable.

1. [ ] Add to the [UX Guide](https://docs.gitlab.com/ce/development/ux_guide/)
1. [ ] Add to the Design Library
1. [ ] Add an agenda item to the next UX weekly call to inform everyone

/label ~"UX"
/cc @cperessini @dimitrieh @hazelyang @pedroms @sarrahvesselov @sarahod @tauriedavis
